import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    Button,
    useDisclosure,
    Input,
    FormLabel,
    FormControl,
    Stack,
    Select,
    FormErrorMessage,
    FormHelperText,
 
  } from '@chakra-ui/react'

import React,{ useState, useRef} from 'react'

export const MyModal = () => {
    const {onClose, isOpen, onOpen} = useDisclosure({defaultIsOpen: true})
    const initialRef = useRef(null)
    const finalRef = useRef(null)
    const [input, setInput] = useState('')
    const handleInputChange = (e) => setInput(e.target.value)
    const isError = input ==='' 
    const [size,setSize] = React.useState('xl') 

    const handleSizeClick = (newSize) => {
      setSize(newSize)
      onOpen()
    }
    const sizes = ['xl']



    return (
        <>
        {sizes.map((size)=>(
          <Button 
          onClick={() =>handleSizeClick(size)}
          key={size}
          m={4}>
            {`Open ${size} Modal`}</Button>))}
          
          <Modal onClose={onClose} size={size} isOpen={isOpen}
          initialFocusRef={initialRef}
          finalFocusRef={finalRef} 
          isOpen={isOpen} 
          onClose={onClose}>
            
          <ModalOverlay
          bg='blackAlpha.300'
          backdropFilter='blur(5px) hue-rotate(90deg)'/>

            <ModalContent>
              <ModalHeader>Создание расчета</ModalHeader>
              <ModalCloseButton />


               <ModalBody>
                <FormControl isInvalid={isError}>
                    <FormLabel  fontWeight='light' fontSize={13}>Название расчета</FormLabel>
                    <Input type='text' value={input} onChange={handleInputChange} placeholder='Добавьте название расчета'/>
                    {!isError ? (
                  <FormHelperText font-size={12}>
                    </FormHelperText>
                    ) : (
                    <FormErrorMessage fontSize={12}>Обязательное поле для заполнения</FormErrorMessage>
                    )}
                </FormControl>

                <FormControl isInvalid={isError} mt={4}>
                    <FormLabel fontWeight='light' fontSize={13}>Описание</FormLabel>
                    <Input type='text' value={input} onChange={handleInputChange} placeholder='Опишите ваш расчет'/>
                    {!isError ?(
                      <FormHelperText font-size={12}>
                      </FormHelperText>
                    ):( 
                      <FormErrorMessage font-size={12}>Обязательное поле для заполнения</FormErrorMessage>
                    )}
                </FormControl>
               
               <FormControl mt={4}>
                    <FormLabel fontWeight='light' fontSize={13}>Проект/Задача</FormLabel>
                    <Stack spacing={3}>
                      <Select variant='filled' placeholder='Выберите проект'/>
                    </Stack>
                </FormControl>

                <FormControl mt={4}>
                    <FormLabel fontWeight='light' fontSize={13}>Шаблоны</FormLabel>
                    <Select placeholder='Текст'>
                      <option value='option2'>Аэродинамика</option>
                      <option value='option3'>Течение капель</option>
                      <option value='option3'>Обледенение</option>
                    </Select>
                </FormControl>

                </ModalBody>

              <ModalFooter>
                <Button colorScheme='purple' mr={3} onClick={onClose}>
                  Далее
                </Button>
                <Button onClick={onClose}>Отмена</Button>
              </ModalFooter>
            </ModalContent>
          </Modal>

        </>
      )
    }
